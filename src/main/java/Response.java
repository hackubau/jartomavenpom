/*
 * Developer:  Marco Guassone - License: GNU GPLv3
 */

import java.util.ArrayList;
import java.util.List;

public class Response {
	 public float numFound;

	 public float start;

	 public List<Artifact> docs = new ArrayList<>();

	 public float getNumFound() {
		  return numFound;
	 }

	 public void setNumFound(float numFound) {
		  this.numFound = numFound;
	 }

	 public float getStart() {
		  return start;
	 }

	 public void setStart(float start) {
		  this.start = start;
	 }

	 public List<Artifact> getDocs() {
		  return docs;
	 }

	 public void setDocs(List<Artifact> docs) {
		  this.docs = docs;
	 }
}
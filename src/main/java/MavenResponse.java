/*
 * Developer:  Marco Guassone - License: GNU GPLv3
 */

public class MavenResponse {
	 ResponseHeader ResponseHeader;
	 Response       Response;

	 public ResponseHeader getResponseHeader() {
		  return ResponseHeader;
	 }

	 public void setResponseHeader(ResponseHeader responseHeader) {
		  ResponseHeader = responseHeader;
	 }

	 public Response getResponse() {
		  return Response;
	 }

	 public void setResponse(Response response) {
		  Response = response;
	 }
}
